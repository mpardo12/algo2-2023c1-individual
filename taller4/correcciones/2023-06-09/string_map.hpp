template<typename T>
string_map<T>::string_map() {
    _size = 0;
    raiz = new Nodo;
}

template <typename T>
string_map<T>::string_map(const string_map<T>& aCopiar) : string_map() {
    *this = aCopiar;
} // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.


template <typename T>
typename::string_map<T>::Nodo* string_map<T>::copiaRaices(Nodo* nodo) {
    if (nodo == nullptr) {
        return nullptr;
    }
    else {
        Nodo* nuevo = new Nodo();
        if ((*nodo).definicion == nullptr) {
            (*nuevo).definicion = nullptr;
        }
        else {
            (*nuevo).definicion = new T(*nodo->definicion);
        }
        for (int clave = 0; clave < nodo->siguientes.size(); clave++) {
            (*nuevo).siguientes[clave] = copiaRaices((*nodo).siguientes[clave]);
        }
        return nuevo;
    }
}

template<typename T>
string_map<T>& string_map<T>::operator=(const string_map<T>& d) {
    destrucNodos(raiz);//Libero memoria del this
    _size = d.size();
    raiz = copiaRaices(d.raiz);
    return *this;
}

template <typename T>
void string_map<T>::destrucNodos(Nodo* nodo) {
    if (nodo->definicion != nullptr){
        delete nodo->definicion;
        nodo->definicion = nullptr;
    }
    for (int i = 0; i <nodo->siguientes.size() ; i++) {
        if(nodo->siguientes[i] != nullptr){
            destrucNodos(nodo->siguientes[i]);
            nodo->siguientes[i] = nullptr;
        }
    }
    delete nodo;
}

template<typename T>
string_map<T>::~string_map() {
    _size = 0;
    if(raiz != nullptr){
        destrucNodos(raiz);
    }
}

template<typename T>
T &string_map<T>::operator[](const string &clave) {
    return raiz->definicion[clave];
}

template<typename T>
void string_map<T>::insert(const pair<string, T> &clave) {
    if (raiz == nullptr) {//Cuando el trie es vacío creo la raiz
        raiz = new Nodo(nullptr);
    }
    int i = 0;
    Nodo* actual = raiz;
    while (i < clave.first.size()) {
        if (actual->siguientes[clave.first[i]] != nullptr) {
            actual = actual->siguientes[clave.first[i]];
            i++;
        }
        else {
            actual->siguientes[clave.first[i]] = new Nodo();
        }
    }
    if (actual->definicion != nullptr) {// Defino el significado
        delete actual->definicion;
    }
    actual->definicion = new T(clave.second);
    _size++;
}

template<typename T>
int string_map<T>::count(const string &clave) const {
    int result = 0;
    if (raiz != nullptr){
        Nodo* actual = raiz;
        int i = 0;
        while (i < clave.size()) {
            if (actual->siguientes[clave[i]] == nullptr) {
                break;
            }
            else {
                actual = actual->siguientes[clave[i]];
                i++;
            }
        }
        if (actual->definicion != nullptr) {
            result = 1;
        }
    }
    return result;
}

template<typename T>
const T &string_map<T>::at(const string &clave) const {
    Nodo* actual = raiz;
    int i = 0;
    while (i < clave.length()){
        actual = (*actual).siguientes[clave[i]];
        i++;
    }
    return ((*actual).definicion);
}


template<typename T>
T &string_map<T>::at(const string &clave) {
    Nodo* actual = raiz;
    int i = 0;
    while (i < clave.length()){
        actual = (*actual).siguientes[clave[i]];
        i++;
    }
    return *((*actual).definicion);
}

template <typename T>
bool string_map<T>:: nodoHoja(Nodo* nodo){
    int i=0;
    while (i<nodo->siguientes.size() && nodo->siguientes[i]== nullptr){
        i++;
    }
    return (i == nodo->siguientes.size());
}

template<typename T>
void string_map<T>::erase(const string &clave) {
    Nodo* actual = raiz;
    vector<Nodo*> recorrido;
    for(int i=0; i<clave.length(); i++){
        recorrido.push_back(actual);
        actual = actual->siguientes[int(clave[i])];
    }
    recorrido.push_back(actual);
    delete(actual->definicion);
    actual->definicion= nullptr;
    if(nodoHoja(actual)) {
        for (int i = recorrido.size()-1; i < 0 ; i--){
            Nodo *anterior = recorrido[i - 1];
            if (actual->definicion == nullptr && nodoHoja(actual)) {
                Nodo *porBorrar = actual;
                actual = anterior;
                anterior->siguientes[int(clave[i])] = nullptr;
                destrucNodos(porBorrar);
            } else {
                break;
            }
        }
    }
    _size--;
}

template <typename T>
int string_map<T>::size() const{
    return _size;
}

template <typename T>
bool string_map<T>::empty() const{
    return (_size == 0);
}
