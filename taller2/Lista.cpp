#include "Lista.h"
#include <cassert>

Lista::Nodo::Nodo(const int &elem) : valor(elem), sig(nullptr), ant(nullptr){}

Lista::Lista() : _primero(nullptr), _ultimo(nullptr),_size(0){}

Lista::Lista(const Lista& l) : Lista() {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.
    *this = l;
}
//La lista que entra (: Lista()) esta vacia y copio la que es por referencia

Lista::~Lista() {
    Nodo* nuevo = _primero;
    while (nuevo != NULL) {
        _primero = _primero->sig;
        delete nuevo;
        nuevo = _primero;
    }
    _size = 0;
}

Lista& Lista::operator=(const Lista& aCopiar) {
    Nodo* nuevo = aCopiar._primero;
    int longitud_original = this->_size;
    for (int k = 0; k < longitud_original; k = k + 1) {
        eliminar(0);
    }
    for (int i = 0; i < aCopiar._size; i = i + 1) {
        agregarAtras(nuevo->valor);
        nuevo = nuevo->sig;
    }
    return *this;
}

void Lista::agregarAdelante(const int& elem) {
    Nodo* nuevo = new Nodo(elem);
    if (_size == 0) {
        _primero = nuevo;
        _ultimo = nuevo;
    } else {
        nuevo->sig = _primero;
        nuevo->sig->ant = nuevo;
        _primero = nuevo;
    }
    _size++;
}

void Lista::agregarAtras(const int& elem) {
    Nodo* nuevo = new Nodo(elem);
    if (_size == 0) {
        _primero = nuevo;
        _ultimo = nuevo;
    } else {
        nuevo->ant = _ultimo;
        nuevo->ant->sig = nuevo;
        _ultimo = nuevo;
    }
    _size++;
}

void Lista::eliminar(Nat i) {
    Nodo* iter = _primero;
    if(i!=0){
        for(int j=0; j<i; j++){
            iter = iter->sig;
        }
        Nodo* prelim = iter->ant;
        prelim->sig = iter->sig;
    }
    else{
        _primero = iter->sig;
    }
    delete iter;
    _size--;
}

int Lista::longitud() const {
    return _size;
}

const int& Lista::iesimo(Nat i) const {
    Nodo* nuevo = _primero;
    for (int k = 1; k <= i; k = k + 1) {
        nuevo = nuevo->sig;
    }
    return nuevo->valor;
    assert(false);
}

int& Lista::iesimo(Nat i) {
    Nodo* nuevo = _primero;
    for (int k = 1; k <= i; k = k + 1) {
        nuevo = nuevo->sig;
    }
    return nuevo->valor;
    assert(false);
}


void Lista::mostrar(ostream& o) {
    // Completar
}