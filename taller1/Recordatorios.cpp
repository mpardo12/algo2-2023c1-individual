#include <iostream>
#include <list>

using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10

// Clase Fecha
class Fecha {
  public:
    Fecha(int mes, int dia);
    int mes();
    int dia();
    bool operator==(Fecha o);
    void incrementar_dia();
    string pasarAString();

  private:
    int _mes;
    int _dia;
};

Fecha::Fecha(int mes, int dia) : _mes(mes), _dia(dia) {};

int Fecha::mes() {
    return _mes;
}

int Fecha::dia() {
    return _dia;
}

ostream& operator<<(ostream& os, Fecha f) {
    os << "" << f.dia() << "/" << f.mes() << "";
    return os;
}

string Fecha::pasarAString() {
    return to_string(_dia) + "/" + to_string(_mes);
}

bool Fecha::operator==(Fecha o) {
    return _dia == o._dia and  _mes == o._mes;;

}

void Fecha::incrementar_dia() {
    _dia++;
    if(_dia > dias_en_mes(_mes)) {
        _dia = 1;
        _mes++;
        if (_mes > 12) {
            _mes = 1;
        }
    }
}



// Ejercicio 11, 12

// Clase Horario

class Horario {
        public:
        Horario(uint hora, uint min);
        uint hora();
        uint min();
        bool operator==(Horario o);
        bool operator<(Horario h);

        private:
        uint _hora;
        uint _min;

};

Horario::Horario(uint hora, uint min) : _hora(hora), _min(min){}

uint Horario:: hora(){
    return _hora;
}

uint Horario::min() {
    return _min;
}

bool Horario::operator==(Horario o) {
    return _hora == o._hora and  _min == o._min;;

}

bool Horario::operator<(Horario h) {
    return _hora < h.hora() or (_hora == h.hora() and _min < h.min());

}

ostream& operator<<(ostream& os, Horario h) {
    os << "" << h.hora() <<":"<< h.min() << "";
    return os;
}

// Ejercicio 13

// Clase Recordatorio
class Recordatorio{
public:
    Recordatorio( Fecha, Horario,string mensaje);
    Fecha fecha();
    Horario horario();
    string mensaje();
    bool operator==(Recordatorio r);

private:
    Fecha _fecha;
    Horario _horario;
    string _mensaje;
};
Recordatorio::Recordatorio(Fecha fecha, Horario horario, string mensaje) : _fecha(fecha), _horario(horario), _mensaje(mensaje){};

Fecha Recordatorio::fecha() {
    return _fecha;
}

Horario Recordatorio::horario() {
    return _horario;
}

string Recordatorio::mensaje() {
    return _mensaje;
}

bool Recordatorio::operator==(Recordatorio r) {
    return _fecha == r.fecha() and _horario == r.horario() and _mensaje == r._mensaje;

}

ostream& operator<<(ostream& os, Recordatorio r) {
    os << "" << r.mensaje() <<" @ "<< r.fecha() << " "  << r.horario() << "";
    return os;
}


// Ejercicio 14

// Clase Agenda

class Agenda {
public:
    Agenda(Fecha fecha_inicial);
    void agregar_recordatorio(Recordatorio rec);
    void incrementar_dia();
    list<Recordatorio> recordatorios_de_hoy();
    Fecha hoy();

private:
    map<string, list<Recordatorio>> _recordatorios_de_hoy;
    Fecha _hoy;
};

Agenda::Agenda(Fecha fecha_inicial) : _hoy(fecha_inicial) {};

Fecha Agenda::hoy() {
    return _hoy;
}

list<Recordatorio> Agenda::recordatorios_de_hoy() {
    list<Recordatorio> listaR = _recordatorios_de_hoy[_hoy.pasarAString()];
    return listaR; 
}

void Agenda::agregar_recordatorio(Recordatorio rec) {
    list<Recordatorio> R = _recordatorios_de_hoy[rec.fecha().pasarAString()];
    auto i = R.begin();
    while(i != R.end()){
        if(rec.horario() < i->horario()){
            R.insert(i,rec);
            break;
        }
        i++;
    }
    if(i==R.end()){
        R.push_back(rec); 
    }
    _recordatorios_de_hoy[rec.fecha().pasarAString()] = R;
}

void Agenda::incrementar_dia() {
    _hoy.incrementar_dia();
}

ostream &operator<<(ostream &os, Agenda a) {
    os << a.hoy() <<endl;
    os << "=====" <<endl;
    for (Recordatorio r: a.recordatorios_de_hoy()) {
         os << r << endl;
    }
    return os;
}

