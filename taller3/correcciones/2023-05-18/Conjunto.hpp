#include "Conjunto.h"

template <class T>
Conjunto<T>::Nodo::Nodo(const T &v) : valor(v), izq(NULL), der(NULL) {}

template <class T>
void Conjunto<T>::destruir(Conjunto::Nodo* raiz) {
    if (raiz != nullptr) {
        destruir(raiz->der);
        destruir(raiz->izq);
        delete raiz;
    }
}

template <class T>
Conjunto<T>::Conjunto() : _raiz(NULL), _cardinal(0) {}

template <class T>
Conjunto<T>::~Conjunto() {
    destruir(_raiz);
}

template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {
    Nodo *actual = _raiz;
    if(_cardinal > 0) {
        while (actual != NULL and (*actual).valor != clave) {
            if (actual != NULL && clave < (*actual).valor) {
                actual = actual->izq;
            }
            if (actual != NULL && clave >  (*actual).valor) {
                actual = actual->der;
            }
        }
        if (actual == NULL) {
            return false;
        }
        else {
            return true;
        }
    }
    else{
        return false;
    }
}

template <class T>
void Conjunto<T>::insertar(const T& clave) {
    if (!pertenece(clave)) {
        Nodo *nuevo = new Nodo(clave);
        Nodo *actual = _raiz;
        if (_cardinal == 0) {
            _raiz = nuevo;
        } else {
            while (actual != nuevo) {
                if (nuevo->valor < actual->valor) {
                    if (actual->izq == NULL) {
                        actual->izq = nuevo;
                    }
                    actual = actual->izq;
                } else if (actual->valor < nuevo->valor) {
                    if (actual->der == NULL) {
                        actual->der = nuevo;
                    }
                    actual = actual->der;
                }
            }
        }
        _cardinal = _cardinal + 1;
    }
}


template <class T>
void Conjunto<T>::remover(const T& clave) {
    if (pertenece(clave)) {
        Nodo *actual = _raiz;
        Nodo *padre = _raiz;
        while ((*actual).valor != NULL) {// Busco el nodo de la clave y su padre
            if (actual->valor == clave) {
                break;
            } else {
                if ((*actual).valor < clave) {
                    padre = actual;
                    actual = actual->der;
                } else {
                    padre = actual;
                    actual = actual->izq;
                }
            }
        }
        if (actual->izq == NULL && actual->der == NULL) { // Elimino una hoja
            if (actual == _raiz) {
                _raiz = NULL;
            } else {
                if (padre->izq == actual) {
                    padre->izq = NULL;
                } else {
                    padre->der = NULL;
                }
            }
            delete actual;
        }
        else {
            if (actual->izq == NULL && actual->der != NULL) {// si tiene hijo derecho
                if (actual == _raiz) {
                    _raiz = actual->der;
                } else {
                    if (actual->valor < padre->valor) {
                        padre->izq = actual->der;
                    } else if (padre->valor < actual->valor) {
                        padre->der = actual->der;
                    }
                    actual->der = NULL;
                    delete actual;
                }
            }
            else {
                if (actual->izq != NULL && actual->der == NULL) {// si tiene hijo izquierdo
                    if (actual == _raiz) {
                        _raiz = actual->izq;
                    } else if (actual->valor < padre->valor) {
                        padre->izq = actual->izq;
                    } else if (padre->valor < actual->valor) {
                        padre->der = actual->izq;
                    }
                    actual->izq = NULL;
                    delete actual;
                }
                else {
                    if (actual->izq != NULL && actual->der != NULL) {// Caso tiene dos hijos
                        int valor_suc = siguiente(actual->valor);
                        Nodo *sucesor = _raiz;
                        while (sucesor->valor != valor_suc) {// Busco el sucesor y armo sus posibles conexiones
                            if (sucesor->valor < valor_suc) {
                                sucesor = sucesor->der;
                            } else if (valor_suc < sucesor->valor) {
                                sucesor = sucesor->izq;
                            }
                        }
                        Nodo *padre_suc = _raiz;
                        while (padre_suc != NULL) {// busco al padre del sucesor
                            if (sucesor == _raiz) {
                                padre_suc = NULL;
                            } else if (padre_suc->izq == sucesor || padre_suc->der == sucesor) {
                                break;
                            } else if (padre_suc->valor < sucesor->valor) {
                                padre_suc = padre_suc->der;
                            } else if (sucesor->valor < padre_suc->valor) {
                                padre_suc = padre_suc->izq;
                            }
                        }
                        if (sucesor->izq == NULL &&
                            sucesor->der == NULL) {// Cuando el sucesor es hoja cambio el nodo del padre por NULL
                            if (padre_suc->izq == sucesor) {
                                padre_suc->izq = NULL;
                            } else {
                                padre_suc->der = NULL;
                            }
                        }
                        else if (sucesor->izq == NULL && sucesor->der != NULL) {// Cuando el sucesor tiene hijo der lo engancho con el padre
                            if (padre_suc->izq == sucesor) {
                                padre_suc->izq = sucesor->der;
                            } else {
                                padre_suc->der = sucesor->der;
                            }
                            sucesor->der = NULL;
                        }
                        else if (sucesor->izq != NULL && sucesor->der == NULL) {// Cuando el sucesor tiene hijo izq lo engancho con el padre
                            if (padre_suc->izq == sucesor) {
                                padre_suc->izq = sucesor->izq;
                            } else {
                                padre_suc->der = sucesor->izq;
                            }
                            sucesor->izq = NULL;
                        }
                        actual->valor = sucesor->valor;
                        delete sucesor;
                    }
                }
            }
        }
        _cardinal = _cardinal - 1;
    }
}


template <class T>
const T& Conjunto<T>::siguiente(const T& clave) {
    Nodo *actual = _raiz;
    Nodo *padre = _raiz;
    while ((*actual).valor != clave) { // Busco el nodo de la clave
        if (actual->valor < clave) {
            padre = actual;
            actual = actual->der;
        } else {
            if (clave < actual->valor) {
                padre = actual;
                actual = actual->izq;
            }
        }
    }
    if (actual->der != NULL) { //Si hay cosas a la derecha es porque el minimo de este subarbol es el siguiente
        actual = actual->der;
        while (actual->izq != NULL) {
            actual = actual->izq;
        }
        return (*actual).valor;
    }
    else {  // si no hay subarbol derecho
        if ((*padre).valor > clave){
            return (*padre).valor;
        }
        else{
            Nodo *sucesor = _raiz;
            while((*sucesor).valor != clave){
                if (clave > sucesor->valor && sucesor->der->valor > clave){
                    return (*sucesor).valor;
                }
                else{
                    if (clave < sucesor->der->valor){
                        padre = sucesor;
                        sucesor = sucesor->izq;
                    }
                    if (sucesor->valor < clave){
                        return (*padre).valor;
                    }
                }
            }
        }
    }
}

template <class T>
const T& Conjunto<T>::minimo() const {
    if(_cardinal > 0) {
        Nodo* actual = _raiz;
        while ((*actual).izq != NULL) {
            actual = (*actual).izq;
        }
        return (*actual).valor;
    }
}

template <class T>
const T& Conjunto<T>::maximo() const {
    if(_cardinal > 0) {
        Nodo* actual = _raiz;
        while ((*actual).der != NULL) {
            actual = (*actual).der;
        }
        return (*actual).valor;
    }
}

template <class T>
unsigned int Conjunto<T>::cardinal() const {
    return _cardinal;
}

template <class T>
void Conjunto<T>::mostrar(std::ostream&) const {
    assert(false);
}

